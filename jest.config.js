module.exports = {
  moduleFileExtensions: ['js', 'ts', 'json', 'vue'],
  transform: {
    '.*\\.(vue)$': 'vue-jest',
    '^.+\\.js$': 'babel-jest',
    '^.+\\.ts$': 'babel-jest',
    '^.+\\.(gql|graphql)$': 'jest-transform-graphql',
  },
  transformIgnorePatterns: ['node_modules/(?!(@gitlab/ui|bootstrap-vue|three)/)'],
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|svg|css)$': '<rootDir>/tests/__mocks__/file_mock.js',
  },
};
