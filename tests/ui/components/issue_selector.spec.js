import { shallowMount } from '@vue/test-utils';
import IssueSelector from '../../../src/ui/components/issue_selector.vue';
import mockIssue from '../mock_data/issue';
import waitForPromises from '../../helpers/wait_for_promises';

describe('Issue component', () => {
  let wrapper;

  function createComponent({ issue = mockIssue, searchInputValue, $apollo } = {}) {
    wrapper = shallowMount(IssueSelector, {
      data() {
        return {
          searchInputValue,
        };
      },
      propsData: {
        issue,
      },
      mocks: {
        $apollo: $apollo || {
          query: jest.fn().mockResolvedValue({ data: { project: { issue: mockIssue } } }),
        },
      },
    });
  }

  describe('with invalid issue URL', () => {
    describe('when URL is not a valid GitLab Issue URL', () => {
      it('does not execute a search', () => {
        const mockOnSearch = jest.fn();
        createComponent({
          searchInputValue: 'https://example.com/issue/1',
          methods: {
            onSearch: mockOnSearch,
          },
        });

        // TODO test that the components watcher on searchInputValue actually works
        wrapper.vm.onSearch();

        return wrapper.vm.$nextTick().then(() => {
          expect(mockOnSearch).not.toHaveBeenCalled();
        });
      });
    });

    describe('when GitLab issue cannot be found', () => {
      it('displays warning to the user', () => {
        createComponent({
          searchInputValue: mockIssue.webUrl + 'asd',
          $apollo: {
            query: jest.fn().mockResolvedValue(), // mock 'issue not found' scenario
          },
        });

        // TODO test that the components watcher on searchInputValue actually works
        wrapper.vm.onSearch();

        return waitForPromises().then(() => {
          expect(wrapper.vm.validIssueUrl).toBe(false); // TODO we should check the DOM
        });
      });
    });
  });

  describe('with no search term', () => {
    it('renders the empty state', () => {
      createComponent();
      expect(wrapper.html()).toMatchSnapshot();
    });
  });

  describe('with valid issue URL', () => {
    it('emits "select" event on successful search', () => {
      createComponent({ searchInputValue: mockIssue.webUrl });

      // TODO test that the components watcher on searchInputValue actually works
      wrapper.vm.onSearch();

      return waitForPromises().then(() => {
        expect(wrapper.emitted().select).toBeTruthy();
        expect(wrapper.emitted().select.length).toBe(1);
        expect(wrapper.emitted().select[0]).toEqual([mockIssue]);
      });
    });
  });
});
