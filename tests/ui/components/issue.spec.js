import { shallowMount } from '@vue/test-utils';
import Issue from '../../../src/ui/components/issue.vue';
import mockIssue from '../mock_data/issue';

describe('Issue component', () => {
  let wrapper;

  function createComponent({ issue = mockIssue } = {}) {
    wrapper = shallowMount(Issue, {
      propsData: {
        issue,
      },
    });
  }

  it('renders the component', () => {
    createComponent();
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('emits click event when clicked', () => {
    createComponent();
    wrapper.trigger('click');
    expect(wrapper.emitted().click).toBeTruthy();
  });

  describe('with a closed issue', () => {
    const closedIssue = {
      ...mockIssue,
      state: 'closed',
    };

    it('renders indication that the issue is closed', () => {
      createComponent({ issue: closedIssue });
      expect(wrapper.html()).toMatchSnapshot();
    });
  });
});
