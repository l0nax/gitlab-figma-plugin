import { shallowMount } from '@vue/test-utils';
import Login from '../../../src/ui/pages/login.vue';

describe('Login page', () => {
  let wrapper;

  function createComponent({ isLoading = false } = {}) {
    wrapper = shallowMount(Login, {
      data() {
        return {
          isLoading,
        };
      },
      stubs: {
        'empty-issue-illustration': true,
      },
    });
  }

  describe('when not loading', () => {
    beforeEach(() => {
      createComponent();
    });

    it('renders login page', () => {
      expect(wrapper.html()).toMatchSnapshot();
    });
  });

  describe('when loading', () => {
    beforeEach(() => {
      createComponent({ isLoading: true });
    });

    it('renders empty screen', () => {
      expect(wrapper.html()).toMatchSnapshot();
    });
  });
});
