import { GITLAB_BASE_URL } from '../../shared/constants';

export const GITLAB_ISSUE_URL_REGEX = /.*\/(.+\/.+)\/-\/issues\/([0-9]+)/;

export function pngFileFromData(data, filename) {
  const fileData = new Blob([data], { type: 'image/png' });
  const file = new File([fileData], `${filename}.png`, {
    type: 'image/png',
  });

  return file;
}

export function parseIssueUrl(issueUrl) {
  const match = issueUrl.match(GITLAB_ISSUE_URL_REGEX);
  if (!issueUrl.startsWith(GITLAB_BASE_URL) || !match) return {};

  const [, projectPath, issueId] = match;
  return {
    projectPath,
    issueId,
  };
}
