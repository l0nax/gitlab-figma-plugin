import { ApolloClient } from 'apollo-client';
import { createUploadLink } from 'apollo-upload-client';
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory';
import gql from 'graphql-tag';
import selectionQuery from './queries/selection.query.graphql';
import accessTokenQuery from './queries/access_token.query.graphql';
import { GITLAB_GRAPHQL_BASE_URL, FIGMA_MESSAGE_TYPES } from '../../shared/constants';

const typeDefs = gql`
  type Selection {
    frames: [String]!
    components: [String]!
  }

  type Mutation {
    setSelection(frames: [String]!, components: [String]!): Selection
    setAccessToken(accessToken: String): Void
  }
`;

const resolvers = {
  Mutation: {
    setSelection: (_, { frames, components }, { cache }) => {
      cache.writeQuery({
        query: selectionQuery,
        data: { selection: { __typename: 'Selection', frames, components } },
      });

      return { frames, components };
    },
    setAccessToken: (_, { accessToken }, { cache }) => {
      cache.writeQuery({
        query: accessTokenQuery,
        data: { accessToken },
      });

      window.parent.postMessage(
        {
          pluginMessage: {
            type: FIGMA_MESSAGE_TYPES.SET_ACCESS_TOKEN,
            data: {
              accessToken,
            },
          },
        },
        '*',
      );

      return;
    },
  },
};

export function createDefaultClient() {
  const cache = new InMemoryCache();
  const authLink = setContext((_, { headers }) => {
    const { accessToken } = cache.readQuery({ query: accessTokenQuery }) || {};
    return {
      headers: {
        ...headers,
        Authorization: `Bearer ${accessToken}`,
      },
    };
  });

  cache.writeData({
    data: {
      selection: null,
      accessToken: null,
    },
  });

  return new ApolloClient({
    typeDefs,
    cache,
    link: authLink.concat(createUploadLink({ uri: GITLAB_GRAPHQL_BASE_URL })),
    resolvers,
  });
}
