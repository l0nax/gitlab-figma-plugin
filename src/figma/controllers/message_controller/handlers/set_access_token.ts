import { setAccessToken } from '../../../utils/figma_utils';

export default function handleSetAccessToken(message: PluginMessage): Promise<void> {
  const { accessToken } = message.data;
  return setAccessToken(accessToken);
}
