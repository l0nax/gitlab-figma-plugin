import { setAccessToken } from '../../../utils/figma_utils';

export default function reset(): void {
  setAccessToken(null).then(() => {
    figma.closePlugin("GitLab Figma Plugin was restored to it's default state.");
  });
}
