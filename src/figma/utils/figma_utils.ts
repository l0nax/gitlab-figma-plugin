import { CLIENT_STORAGE_KEYS } from '../constants';

/**
 * Return all Figma Frames and Components that are currently selected by the user in Figma
 */
export const getCurrentSelection = (): PluginSelection => {
  const currentSelection = figma.currentPage.selection || [];

  const validSelection: PluginSelection = { components: [], frames: [] };
  currentSelection.forEach(selection => {
    if (selection.type === 'FRAME') {
      validSelection.frames.push(selection);
    } else if (selection.type === 'COMPONENT') {
      validSelection.components.push(selection);
    }
  });

  return validSelection;
};

export const setAccessToken = (accessToken: string): Promise<void> => {
  return figma.clientStorage.setAsync(CLIENT_STORAGE_KEYS.GITLAB_ACCESS_TOKEN, accessToken);
};

export const getAccessToken = (): Promise<string | null> => {
  return figma.clientStorage.getAsync(CLIENT_STORAGE_KEYS.GITLAB_ACCESS_TOKEN);
};
