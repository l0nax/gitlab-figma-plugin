export const MENU_COMMANDS = {
  SHOW_UPLOAD_UI: 'show_upload_ui',
  RESET: 'reset',
};

export const CLIENT_STORAGE_KEYS = {
  GITLAB_ACCESS_TOKEN: 'GITLAB_ACCESS_TOKEN',
};
